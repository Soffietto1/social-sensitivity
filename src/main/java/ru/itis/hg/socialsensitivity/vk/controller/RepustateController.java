package ru.itis.hg.socialsensitivity.vk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hg.socialsensitivity.vk.dao.SentimentQueryBean;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.service.RepustateService;
import ru.itis.hg.socialsensitivity.vk.service.VkUserInfoService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class RepustateController {

    @Autowired
    private RepustateService repustateService;
    @Autowired
    private VkUserInfoService userInfoService;

    @PostMapping(value = "/sentiment")
    public ResponseEntity<SentimentDto> getTextSentiment(@RequestParam("text") String text) {
        return ResponseEntity.ok(repustateService.getSentimentByText(text));
    }

    @GetMapping(value = "/sentiment/{user_id}")
    public ResponseEntity<SentimentDto> getSentimentByUser(@RequestParam("user_id") Long userId) {
        String text = repustateService.getTextByUserId(userId);
        return ResponseEntity.ok(repustateService.getSentimentByText(text));
    }

    @GetMapping(value = "/sentiment/all")
    public ResponseEntity<List<SentimentDto>> getSentimentForAllUsers() {
        List<User> allUsers = userInfoService.getAll();
        List<SentimentDto> sentimentDtos = new ArrayList<>();
        for (User user : allUsers) {
            String text = repustateService.getTextByUserId(user.getId());
            SentimentDto sentimentByText = repustateService.getSentimentByText(text);
            sentimentDtos.add(sentimentByText);
            repustateService.save(sentimentByText, user, text);
        }
        return ResponseEntity.ok(sentimentDtos);
    }
}
