package ru.itis.hg.socialsensitivity.vk.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentChooseDto;
import ru.itis.hg.socialsensitivity.vk.dto.enums.Sentiment;
import ru.itis.hg.socialsensitivity.vk.entity.SentimentDictionary;
import ru.itis.hg.socialsensitivity.vk.service.impl.SentimentDictionaryService;
import ru.itis.hg.socialsensitivity.vk.util.RandomWordsUtil;

import java.util.List;

@Controller
@RequiredArgsConstructor
public class SentimentController {

    private final SentimentDictionaryService sentimentDictionaryService;

    @GetMapping("/sentiment/dictionary")
    public String getSentimentPage(Model model) {
        String randomWord = RandomWordsUtil.getRandomWord();
        model.addAttribute("word", randomWord);
        model.addAttribute("sentiments", Sentiment.values());
        SentimentChooseDto sentimentChooseDto = new SentimentChooseDto();
        sentimentChooseDto.setWord(randomWord);
        model.addAttribute("sentimentDto", sentimentChooseDto);
        return "sentiment";
    }

    @PostMapping("/sentiment/dictionary")
    public String getSentimentPage(@ModelAttribute("sentimentDto") SentimentChooseDto sentimentChooseDto) {
        sentimentDictionaryService.save(sentimentChooseDto);
        return "redirect:/sentiment/dictionary";
    }

    @GetMapping("/sentiment/dictionary/top")
    public String getSentimentTop(Model model) {
        List<SentimentDictionary> topWords = sentimentDictionaryService.findTopWords();
        model.addAttribute("words", topWords);
        return "sentiment_top";
    }
}
