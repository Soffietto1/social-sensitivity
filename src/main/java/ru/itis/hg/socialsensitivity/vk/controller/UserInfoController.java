package ru.itis.hg.socialsensitivity.vk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hg.socialsensitivity.vk.dto.AllUserInfoDto;
import ru.itis.hg.socialsensitivity.vk.dto.CommentDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;
import ru.itis.hg.socialsensitivity.vk.service.VkCommentsService;
import ru.itis.hg.socialsensitivity.vk.service.VkUserInfoService;
import ru.itis.hg.socialsensitivity.vk.service.VkUserPostService;

/**
 * @author Azat Khayrullin
 */
@RestController
public class UserInfoController {

    @Autowired
    private VkUserInfoService userInfoService;
    @Autowired
    private VkUserPostService userPostService;
    @Autowired
    private VkCommentsService commentsService;

    @GetMapping("/vk/user/{user_ids}")
    public ResponseEntity<UserInfoDto[]> getUserByIds(@PathVariable("user_ids") String ids) {
        UserInfoDto[] userInfoDtos = userInfoService.getUsersByIds(ids);
        userInfoService.saveUser(userInfoDtos);
        return ResponseEntity.ok(userInfoDtos);
    }

    @GetMapping("/vk/user/{owner_id}/posts")
    public ResponseEntity<UserPostDto[]> getUserPostsByOwnerId(@PathVariable("owner_id") String ownerId) {
        UserPostDto[] userPosts = userPostService.getUserPosts(ownerId);
        userPostService.savePost(userPosts);
        return ResponseEntity.ok(userPosts);
    }

    @GetMapping("/vk/user/{user_id}/friends")
    public ResponseEntity<UserInfoDto[]> getUserFriends(@PathVariable("user_id") String userId,
                                                        @RequestParam int count) {
        UserInfoDto[] userInfoDtos = userInfoService.getUsersFriends(userId, count);
        userInfoService.saveUser(userInfoDtos);
        return ResponseEntity.ok(userInfoDtos);
    }

    @GetMapping("/vk/user/{user_id}/{post_id}/comments")
    public ResponseEntity<CommentDto[]> getUserPostComments(@PathVariable("user_id") Long userId,
                                                            @PathVariable("post_id") Long postId) {
        CommentDto[] commentDtos = commentsService.getComments(userId, postId);
        commentsService.saveComments(commentDtos, postId);
        return ResponseEntity.ok(commentDtos);
    }

    @GetMapping("/vk/{user_ids}/all_info")
    public ResponseEntity<AllUserInfoDto> getAllUserInfoDto(@PathVariable("user_ids") String userIds) {
        AllUserInfoDto allUserInfoDto = new AllUserInfoDto();
        UserInfoDto[] userInfoDtos = userInfoService.getUsersByIds(userIds);
        userInfoService.saveUser(userInfoDtos);
        allUserInfoDto.setUserInfoDtos(userInfoDtos);
        for (UserInfoDto user : userInfoDtos) {
            UserPostDto[] userPostDtos = userPostService.getUserPosts(String.valueOf(user.getId()));
            userPostService.savePost(userPostDtos);
            allUserInfoDto.getUserPostDtos().add(userPostDtos);
            for (UserPostDto posts : userPostDtos) {
                CommentDto[] commentDtos = commentsService.getComments(user.getId(), posts.getId());
                allUserInfoDto.getUserCommentsDto().add(commentDtos);
                commentsService.saveComments(commentDtos, posts.getId());
            }
            UserInfoDto[] friends = userInfoService.getUsersFriends(String.valueOf(user.getId()), 100);
            userInfoService.saveUser(friends);
            for (UserInfoDto friend : friends) {
                UserPostDto[] userFriendPostDtos = userPostService.getUserPosts(String.valueOf(friend.getId()));
                userPostService.savePost(userFriendPostDtos);
                allUserInfoDto.getUserPostDtos().add(userFriendPostDtos);
                for (UserPostDto posts : userFriendPostDtos) {
                    CommentDto[] commentDtos = commentsService.getComments(friend.getId(), posts.getId());
                    allUserInfoDto.getUserCommentsDto().add(commentDtos);
                    commentsService.saveComments(commentDtos, posts.getId());
                }
            }
        }
        return ResponseEntity.ok(allUserInfoDto);
    }
}
