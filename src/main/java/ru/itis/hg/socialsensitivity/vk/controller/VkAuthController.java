package ru.itis.hg.socialsensitivity.vk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;
import ru.itis.hg.socialsensitivity.vk.service.VkAuthService;

/**
 * @author Azat Khayrullin
 */
@Controller
public class VkAuthController {

    @Autowired
    private VkAuthService authService;

    @Autowired
    private VkApi vkApi;

    @GetMapping("/vk/auth")
    public String auth() {
        return "redirect:" + vkApi.getFullAuthUri();
    }

    @GetMapping("/vk/test")
    @ResponseBody
    public ResponseEntity<String> authTest(@RequestParam(value = "code", required = false) String code) {
        if(code == null) {
            return new ResponseEntity<>("Code is empty", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        authService.initAccessToken(code);
        return ResponseEntity.ok(authService.getToken());
    }
}
