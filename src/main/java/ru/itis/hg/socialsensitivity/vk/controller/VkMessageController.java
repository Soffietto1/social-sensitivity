package ru.itis.hg.socialsensitivity.vk.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.hg.socialsensitivity.vk.dto.VkMessageDto;
import ru.itis.hg.socialsensitivity.vk.service.VkDialogService;

@RestController
public class VkMessageController {

    @Autowired
    private VkDialogService vkDialogService;

    @GetMapping("/vk/{user_id}/messages")
    public ResponseEntity<VkMessageDto[]> getMessagesByPeerId(@PathVariable(name = "user_id") Long userId) {
        VkMessageDto[] messageDtos = vkDialogService.getMessagesByDialogId(userId);
        vkDialogService.saveMessage(messageDtos);
        return ResponseEntity.ok(messageDtos);
    }
}
