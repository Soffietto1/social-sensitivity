package ru.itis.hg.socialsensitivity.vk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.hg.socialsensitivity.vk.entity.VkComment;

import java.util.List;

public interface CommentRepository extends JpaRepository<VkComment, Long> {

    @Query(value = "SELECT c.text from vk_comment c WHERE c.from_id=?", nativeQuery = true)
    List<String> getTextByFromId(Long fromId);
}
