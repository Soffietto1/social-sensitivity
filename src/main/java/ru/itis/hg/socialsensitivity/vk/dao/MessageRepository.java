package ru.itis.hg.socialsensitivity.vk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.hg.socialsensitivity.vk.entity.VkMessage;

public interface MessageRepository extends JpaRepository<VkMessage, Long> {
}
