package ru.itis.hg.socialsensitivity.vk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.hg.socialsensitivity.vk.entity.UserPost;

import java.util.List;

/**
 * @author Azat Khayrullin
 */
public interface PostRepository extends JpaRepository<UserPost, Long> {

    @Query(value = "SELECT p.text FROM user_post p WHERE p.user_id=?", nativeQuery = true)
    List<String> getTextByUserId(Long userId);
}
