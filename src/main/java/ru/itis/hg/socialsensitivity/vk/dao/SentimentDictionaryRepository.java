package ru.itis.hg.socialsensitivity.vk.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.hg.socialsensitivity.vk.entity.SentimentDictionary;

import java.util.List;
import java.util.Optional;

public interface SentimentDictionaryRepository extends JpaRepository<SentimentDictionary, Long> {
    Optional<SentimentDictionary> findOneByWord(String word);

    List<SentimentDictionary> findAllByOrderBySentimentDesc();
}
