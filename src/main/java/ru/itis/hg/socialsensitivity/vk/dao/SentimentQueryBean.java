package ru.itis.hg.socialsensitivity.vk.dao;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
public class SentimentQueryBean {
    private Long id;
    private Long userId;
    private String firstName;
    private String lastName;
    private Date date;
    private double score;
    private int wordsCount;
}
