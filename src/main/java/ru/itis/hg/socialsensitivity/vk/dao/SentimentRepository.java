package ru.itis.hg.socialsensitivity.vk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.itis.hg.socialsensitivity.vk.entity.UserSentiment;

import javax.persistence.Tuple;
import java.util.List;

public interface SentimentRepository extends JpaRepository<UserSentiment, Long> {
}
