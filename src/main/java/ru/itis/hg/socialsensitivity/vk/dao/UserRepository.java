package ru.itis.hg.socialsensitivity.vk.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.hg.socialsensitivity.vk.entity.User;

import java.util.List;

/**
 * @author Azat Khayrullin
 */
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();
}
