package ru.itis.hg.socialsensitivity.vk.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter()
public class AllUserInfoDto {
    private UserInfoDto[] userInfoDtos;
    private List<UserPostDto[]> userPostDtos;
    private List<CommentDto[]> userCommentsDto;

    public AllUserInfoDto() {
        this.userPostDtos = new ArrayList<>();
        this.userCommentsDto = new ArrayList<>();
    }
}
