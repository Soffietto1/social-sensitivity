package ru.itis.hg.socialsensitivity.vk.dto;

import lombok.Getter;
import lombok.Setter;
import ru.itis.hg.socialsensitivity.vk.dto.enums.Sentiment;

@Getter
@Setter
public class SentimentChooseDto {
    private String word;
    private Sentiment sentiment;
}
