package ru.itis.hg.socialsensitivity.vk.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SentimentDto {
    private String status;
    private double score;

}
