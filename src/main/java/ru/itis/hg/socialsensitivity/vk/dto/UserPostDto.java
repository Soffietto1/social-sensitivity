package ru.itis.hg.socialsensitivity.vk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Azat Khayrullin
 */
@Getter
@Setter
public class UserPostDto {
    private Long id;
    private String text;
    private Long date;
    @JsonProperty("owner_id")
    private Long ownerId;
}
