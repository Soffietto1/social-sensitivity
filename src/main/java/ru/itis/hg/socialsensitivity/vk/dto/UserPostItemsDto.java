package ru.itis.hg.socialsensitivity.vk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Azat Khayrullin
 */
@Getter
@Setter
public class UserPostItemsDto {
    @JsonProperty("items")
    private UserPostDto[] items;
}
