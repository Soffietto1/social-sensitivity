package ru.itis.hg.socialsensitivity.vk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Azat Khayrullin
 */
@Getter
@Setter
public class UserPostResponseDto {
    @JsonProperty("response")
    private UserPostItemsDto userPostDtos;
}
