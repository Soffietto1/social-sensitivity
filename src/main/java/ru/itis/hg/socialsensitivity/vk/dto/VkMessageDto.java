package ru.itis.hg.socialsensitivity.vk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VkMessageDto {
    private Long date;
    @JsonProperty("from_id")
    private Long fromId;
    private String text;
    @JsonProperty("peer_id")
    private Long peerId;
}
