package ru.itis.hg.socialsensitivity.vk.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VkMessageResponseDto {
    @JsonProperty("response")
    private VkMessagesItemsDto itemsDto;
}
