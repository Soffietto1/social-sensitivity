package ru.itis.hg.socialsensitivity.vk.dto.enums;

public enum Sentiment {
    VERY_SAD("Очень грусное", -1),
    SAD("Грусное", -0.5),
    NEUTRAL("Нейтральное", 0),
    HAPPY("Веселое", 0.5),
    VERY_HAPPY("Очень веселое", 1);

    private String name;
    private double sentimentInt;

    Sentiment(String name, double sentimentInt) {
        this.name = name;
        this.sentimentInt = sentimentInt;
    }

    public double getSentimentInt() {
        return sentimentInt;
    }

    public String getName() {
        return name;
    }
}
