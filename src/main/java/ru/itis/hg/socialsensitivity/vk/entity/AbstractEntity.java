package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * @author Azat Khayrullin
 */
@MappedSuperclass
@Getter
@Setter
class AbstractEntity {
    @Id
    private Long id;
}
