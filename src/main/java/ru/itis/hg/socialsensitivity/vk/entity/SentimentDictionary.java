package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.itis.hg.socialsensitivity.vk.dto.enums.Sentiment;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SentimentDictionary {
    @Id
    @GeneratedValue
    private Long id;
    private String word;
    private double sentiment;
    private int wordCount;
}
