package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.*;
import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Azat Khayrullin
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Table(name = "users")
public class User extends AbstractEntity{
    private String firstName;
    private String lastName;
}
