package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.*;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * @author Azat Khayrullin
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UserPost extends AbstractEntity {
    private Long userId;
    @Column(columnDefinition="TEXT")
    private String text;
    private Long date;
}
