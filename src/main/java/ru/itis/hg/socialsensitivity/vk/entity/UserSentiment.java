package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class UserSentiment {
    @Id
    @GeneratedValue
    private Long id;
    private Long userId;
    private String firstName;
    private String lastName;
    private double score;
    private Date date;
    private int wordCount;
}
