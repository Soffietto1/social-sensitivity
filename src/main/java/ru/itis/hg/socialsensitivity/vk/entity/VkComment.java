package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class VkComment extends AbstractEntity {
    private Long pageId;
    private Long fromId;
    private Long date;
    @Column(columnDefinition="TEXT")
    private String text;
}
