package ru.itis.hg.socialsensitivity.vk.entity;

import lombok.*;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class VkMessage extends AbstractEntity {
    private Long fromId;
    private Long dialogId;
    private String text;
    private Long date;
}
