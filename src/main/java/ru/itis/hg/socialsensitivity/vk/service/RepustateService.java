package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dao.SentimentQueryBean;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;

import java.util.List;

public interface RepustateService {
    public static final String API_KEY = "c0120b814c5b8df052ae9d2331780b0f1982f74d";

    SentimentDto getSentimentByText(String text);

    String getTextByUserId(Long userId);

    void save(SentimentDto sentimentDto, User user, String text);
}
