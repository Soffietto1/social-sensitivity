package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dto.VkTokenDto;

/**
 * @author Azat Khayrullin
 */
public interface VkAuthService {
    void initAccessToken(String code);

    VkTokenDto getTokenInfo();

    default String getToken() {
        return getTokenInfo().getAccessToken();
    }
}
