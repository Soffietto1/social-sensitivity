package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dto.CommentDto;

public interface VkCommentsService {
    CommentDto[] getComments(Long ownerId, Long postId);
    void saveComments(CommentDto[] commentDtos, Long postId);
}
