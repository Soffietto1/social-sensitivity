package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dto.VkMessageDto;
import ru.itis.hg.socialsensitivity.vk.entity.VkDialog;
import ru.itis.hg.socialsensitivity.vk.entity.VkMessage;

import java.util.List;

public interface VkDialogService {
    VkMessageDto[] getMessagesByDialogId(Long dialogId);
    VkDialog[] getDialogs();
    void saveMessage(VkMessageDto vkMessage);
    void saveMessage(VkMessageDto[] vkMessages);
}
