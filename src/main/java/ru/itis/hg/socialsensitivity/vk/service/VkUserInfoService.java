package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;

import java.util.List;

/**
 * @author Azat Khayrullin
 */
public interface VkUserInfoService {

    UserInfoDto[] getUsersByIds(String ids);
    UserInfoDto[] getUsersFriends(String userId, int friendsCount);
    void saveUser(User user);
    void saveUser(UserInfoDto userInfoDto);
    void saveUser(UserInfoDto[] userInfoDtos);
    List<User> getAll();
}
