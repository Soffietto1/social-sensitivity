package ru.itis.hg.socialsensitivity.vk.service;

import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.entity.UserPost;

/**
 * @author Azat Khayrullin
 */
public interface VkUserPostService {
    UserPostDto[] getUserPosts(String ownerId);
    void savePost(UserPost userPost);
    void savePost(UserPostDto userPostDto);
    void savePost(UserPostDto[] userPostDtos);
}
