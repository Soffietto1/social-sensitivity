package ru.itis.hg.socialsensitivity.vk.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dao.CommentRepository;
import ru.itis.hg.socialsensitivity.vk.dao.PostRepository;
import ru.itis.hg.socialsensitivity.vk.dao.SentimentRepository;
import ru.itis.hg.socialsensitivity.vk.dao.UserRepository;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.service.RepustateService;
import ru.itis.hg.socialsensitivity.vk.util.SentimentUtil;

import java.util.List;

@Service
public class RepustateServiceImpl implements RepustateService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private SentimentRepository sentimentRepository;


    @Override
    public SentimentDto getSentimentByText(String text) {
        try {
            MultiValueMap<String, String> parametersMap = new LinkedMultiValueMap<>();
            parametersMap.add("text", text);
            parametersMap.add("lang", "ru");
            String response = restTemplate.postForObject("https://api.repustate.com/v3/" + API_KEY + "/score.json", parametersMap, String.class);
            return mapper.readValue(response, SentimentDto.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getTextByUserId(Long userId) {
        List<String> postsText = postRepository.getTextByUserId(userId);
        List<String> commentsText = commentRepository.getTextByFromId(userId);
        String postTextString = String.join(" ", postsText);
        String commentsTextString = String.join(" ", commentsText);
        return postTextString + " " + commentsTextString;
    }

    @Override
    public void save(SentimentDto sentimentDto, User user, String text) {
        int wordsCount = text.trim().split(" ").length;
        sentimentRepository.save(SentimentUtil.toUserSentiment(sentimentDto, user, wordsCount));
    }
}
