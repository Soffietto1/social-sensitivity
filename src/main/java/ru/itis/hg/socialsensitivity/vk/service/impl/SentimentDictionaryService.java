package ru.itis.hg.socialsensitivity.vk.service.impl;

import ru.itis.hg.socialsensitivity.vk.dto.SentimentChooseDto;
import ru.itis.hg.socialsensitivity.vk.entity.SentimentDictionary;

import java.util.List;

public interface SentimentDictionaryService {

    void save(SentimentChooseDto sentimentChooseDto);

    List<SentimentDictionary> findTopWords();
}
