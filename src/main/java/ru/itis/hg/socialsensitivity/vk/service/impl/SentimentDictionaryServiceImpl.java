package ru.itis.hg.socialsensitivity.vk.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.hg.socialsensitivity.vk.dao.SentimentDictionaryRepository;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentChooseDto;
import ru.itis.hg.socialsensitivity.vk.entity.SentimentDictionary;
import ru.itis.hg.socialsensitivity.vk.util.SentimentUtil;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SentimentDictionaryServiceImpl implements SentimentDictionaryService {

    private final SentimentDictionaryRepository sentimentDictionaryRepository;

    @Override
    public void save(SentimentChooseDto sentimentChooseDto) {
        Optional<SentimentDictionary> oneByWord = sentimentDictionaryRepository.findOneByWord(sentimentChooseDto.getWord());
        SentimentDictionary sentimentDictionary;
        if(oneByWord.isPresent()) {
            sentimentDictionary = oneByWord.get();
            SentimentUtil.updateSentimentDictionary(sentimentDictionary, sentimentChooseDto);
        } else {
            sentimentDictionary = SentimentUtil.toSentimentDictionary(sentimentChooseDto);
        }
        sentimentDictionaryRepository.save(sentimentDictionary);
    }

    @Override
    public List<SentimentDictionary> findTopWords() {
        return sentimentDictionaryRepository.findAllByOrderBySentimentDesc();
    }
}
