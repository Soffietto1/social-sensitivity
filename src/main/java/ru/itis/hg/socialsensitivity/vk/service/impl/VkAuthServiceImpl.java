package ru.itis.hg.socialsensitivity.vk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dto.VkTokenDto;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;
import ru.itis.hg.socialsensitivity.vk.service.VkAuthService;

/**
 * @author Azat Khayrullin
 */
@Service
public class VkAuthServiceImpl implements VkAuthService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VkApi vkApi;

    private VkTokenDto token;

    @Override
    public void initAccessToken(String code) {
        token = restTemplate.getForEntity(vkApi.getAccessTokeUri(code), VkTokenDto.class).getBody();
        vkApi.setUserToken(token.getAccessToken());
    }

    @Override
    public VkTokenDto getTokenInfo() {
        return token;
    }
}
