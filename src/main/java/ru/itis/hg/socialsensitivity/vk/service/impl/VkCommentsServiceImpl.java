package ru.itis.hg.socialsensitivity.vk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dao.CommentRepository;
import ru.itis.hg.socialsensitivity.vk.dto.CommentDto;
import ru.itis.hg.socialsensitivity.vk.dto.CommentResponseDto;
import ru.itis.hg.socialsensitivity.vk.entity.VkComment;
import ru.itis.hg.socialsensitivity.vk.service.VkCommentsService;
import ru.itis.hg.socialsensitivity.vk.util.CommentUtil;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class VkCommentsServiceImpl implements VkCommentsService {

    @Autowired
    private CommentRepository repository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VkApi vkApi;

    @Override
    public CommentDto[] getComments(Long ownerId, Long postId) {
        String url = vkApi.getUserPostCommentUri(ownerId, postId);
        CommentResponseDto commentResponseDto = restTemplate.getForEntity(url, CommentResponseDto.class).getBody();
        if (commentResponseDto == null || commentResponseDto.getCommentItemsDto() == null) {
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            commentResponseDto = restTemplate.getForEntity(url, CommentResponseDto.class).getBody();
            if (commentResponseDto == null || commentResponseDto.getCommentItemsDto() == null) {
                return new CommentDto[0];
            }
        }
        return commentResponseDto.getCommentItemsDto().getCommentDtos();
    }

    @Override
    public void saveComments(CommentDto[] commentDtos, Long postId) {
        List<VkComment> comments = CommentUtil.toComments(commentDtos, postId).stream()
                .filter(Objects::nonNull)
                .filter(comment -> comment.getText() != null)
                .filter(comment -> !comment.getText().isEmpty())
                .collect(Collectors.toList());
        repository.saveAll(comments);
    }
}
