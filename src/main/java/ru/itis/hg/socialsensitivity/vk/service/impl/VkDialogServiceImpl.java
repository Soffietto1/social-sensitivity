package ru.itis.hg.socialsensitivity.vk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dao.MessageRepository;
import ru.itis.hg.socialsensitivity.vk.dto.VkMessageDto;
import ru.itis.hg.socialsensitivity.vk.dto.VkMessageResponseDto;
import ru.itis.hg.socialsensitivity.vk.entity.VkDialog;
import ru.itis.hg.socialsensitivity.vk.entity.VkMessage;
import ru.itis.hg.socialsensitivity.vk.service.VkDialogService;
import ru.itis.hg.socialsensitivity.vk.util.MessageUtil;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VkDialogServiceImpl implements VkDialogService {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VkApi vkApi;

    @Override
    public VkMessageDto[] getMessagesByDialogId(Long dialogId) {
        String messageHistoryUrl = vkApi.getDialogHistoryUrl(dialogId);
        VkMessageResponseDto vkMessageResponseDto = restTemplate.getForEntity(messageHistoryUrl, VkMessageResponseDto.class).getBody();
        return vkMessageResponseDto.getItemsDto().getMessageDtos();
    }

    @Override
    public VkDialog[] getDialogs() {
        return null;
    }

    @Override
    public void saveMessage(VkMessageDto vkMessage) {
        messageRepository.save(MessageUtil.toMessage(vkMessage));
    }

    @Override
    public void saveMessage(VkMessageDto[] vkMessages) {
        List<VkMessage> vkMessagesEntities = MessageUtil.toMessages(vkMessages)
                .stream()
                .filter(vkMessage -> !vkMessage.getText().isEmpty())
                .collect(Collectors.toList());
        messageRepository.saveAll(vkMessagesEntities);
    }
}
