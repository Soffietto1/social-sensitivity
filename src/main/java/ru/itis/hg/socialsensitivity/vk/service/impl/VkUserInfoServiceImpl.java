package ru.itis.hg.socialsensitivity.vk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dao.PostRepository;
import ru.itis.hg.socialsensitivity.vk.dao.UserRepository;
import ru.itis.hg.socialsensitivity.vk.dto.*;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.util.UserUtil;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;
import ru.itis.hg.socialsensitivity.vk.service.VkAuthService;
import ru.itis.hg.socialsensitivity.vk.service.VkUserInfoService;

import java.util.Arrays;
import java.util.List;

/**
 * @author Azat Khayrullin
 */
@Service
public class VkUserInfoServiceImpl implements VkUserInfoService {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VkApi vkApi;
    @Autowired
    private UserRepository repository;

    @Override
    public UserInfoDto[] getUsersByIds(String ids) {
        String url = vkApi.getFindUserUri(ids);
        UserInfoResponseDto responseDto = restTemplate.getForEntity(url, UserInfoResponseDto.class).getBody();
        return responseDto.getUserInfoDtos();
    }

    @Override
    public UserInfoDto[] getUsersFriends(String userId, int friendsCount) {
        String url = vkApi.getFriendsUri(userId, friendsCount);
        UserFriendsResponseDto responseDto = restTemplate.getForEntity(url, UserFriendsResponseDto.class).getBody();
        return responseDto.getUserFriendsItemsDto().getUserInfoDtos();
    }

    @Override
    public void saveUser(User user) {
        repository.save(user);
    }

    @Override
    public void saveUser(UserInfoDto userInfoDto) {
        User user = UserUtil.toUser(userInfoDto);
        repository.save(user);
    }

    @Override
    public void saveUser(UserInfoDto[] userInfoDtos) {
        repository.saveAll(UserUtil.toUsers(userInfoDtos));
    }

    @Override
    public List<User> getAll() {
        return repository.findAll();
    }
}
