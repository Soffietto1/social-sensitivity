package ru.itis.hg.socialsensitivity.vk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.itis.hg.socialsensitivity.vk.dao.PostRepository;
import ru.itis.hg.socialsensitivity.vk.dao.UserRepository;
import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;
import ru.itis.hg.socialsensitivity.vk.dto.UserPostResponseDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.entity.UserPost;
import ru.itis.hg.socialsensitivity.vk.service.VkUserPostService;
import ru.itis.hg.socialsensitivity.vk.util.PostUtil;
import ru.itis.hg.socialsensitivity.vk.util.VkApi;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author Azat Khayrullin
 */
@Service
public class VkUserPostServiceImpl implements VkUserPostService {

    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private VkApi vkApi;
    @Autowired
    private PostRepository repository;

    @Override
    public UserPostDto[] getUserPosts(String ownerId) {
        String url = vkApi.getUserPostUri(ownerId);
        UserPostResponseDto responseDto = restTemplate.getForEntity(url, UserPostResponseDto.class).getBody();
        if(responseDto == null || responseDto.getUserPostDtos() == null) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            responseDto = restTemplate.getForEntity(url, UserPostResponseDto.class).getBody();
            if(responseDto == null || responseDto.getUserPostDtos() == null) {
                return new UserPostDto[0];
            }
            }
        return responseDto.getUserPostDtos().getItems();
    }

    @Override
    public void savePost(UserPost userPost) {
        repository.save(userPost);
    }

    @Override
    public void savePost(UserPostDto userPostDto) {
        UserPost userPost = PostUtil.toPost(userPostDto);
        repository.save(userPost);
    }

    @Override
    public void savePost(UserPostDto[] userPostDtos) {
        UserPostDto[] filteredPosts = Arrays.stream(userPostDtos)
                .filter(userPostDto -> !userPostDto.getText().isEmpty())
                .toArray(UserPostDto[]::new);
        repository.saveAll(PostUtil.toPosts(filteredPosts));
    }
}
