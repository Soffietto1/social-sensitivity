package ru.itis.hg.socialsensitivity.vk.util;

import ru.itis.hg.socialsensitivity.vk.dto.CommentDto;
import ru.itis.hg.socialsensitivity.vk.entity.VkComment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CommentUtil {
    public static VkComment toComment(CommentDto commentDto, Long pageId) {
        VkComment build = VkComment.builder()
                .date(commentDto.getDate())
                .fromId(commentDto.getFromId())
                .pageId(pageId)
                .text(commentDto.getText())
                .build();
        build.setId(commentDto.getId());
        return build;
    }

    public static List<VkComment> toComments(CommentDto[] commentDtos, Long pageId) {
        return Arrays.stream(commentDtos)
                .map(commentDto -> toComment(commentDto, pageId))
                .collect(Collectors.toList());
    }
}
