package ru.itis.hg.socialsensitivity.vk.util;

import ru.itis.hg.socialsensitivity.vk.dto.VkMessageDto;
import ru.itis.hg.socialsensitivity.vk.entity.VkMessage;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MessageUtil {

    public static VkMessage toMessage(VkMessageDto messageDto) {
        return VkMessage.builder()
                .date(messageDto.getDate())
                .fromId(messageDto.getFromId())
                .dialogId(messageDto.getPeerId())
                .text(messageDto.getText())
                .build();
    }

    public static List<VkMessage> toMessages(VkMessageDto[] messageDtos) {
        return Arrays.stream(messageDtos)
                .map(MessageUtil::toMessage)
                .collect(Collectors.toList());
    }
}
