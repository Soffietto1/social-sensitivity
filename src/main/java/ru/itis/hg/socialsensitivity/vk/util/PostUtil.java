package ru.itis.hg.socialsensitivity.vk.util;

import ru.itis.hg.socialsensitivity.vk.dto.UserPostDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.entity.UserPost;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PostUtil {
    public static UserPost toPost(UserPostDto userPostDto) {
        UserPost post = UserPost.builder()
                .date(userPostDto.getDate())
                .userId(userPostDto.getOwnerId())
                .text(userPostDto.getText())
                .build();
        post.setId(userPostDto.getId());
        return post;
    }

    public static List<UserPost> toPosts(UserPostDto[] userPostDtos) {
        return Arrays.stream(userPostDtos)
                .map(PostUtil::toPost)
                .collect(Collectors.toList());
    }
}
