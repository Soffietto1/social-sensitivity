package ru.itis.hg.socialsensitivity.vk.util;

import ru.itis.hg.socialsensitivity.vk.dto.SentimentChooseDto;
import ru.itis.hg.socialsensitivity.vk.dto.SentimentDto;
import ru.itis.hg.socialsensitivity.vk.entity.SentimentDictionary;
import ru.itis.hg.socialsensitivity.vk.entity.User;
import ru.itis.hg.socialsensitivity.vk.entity.UserSentiment;

import java.util.Date;

public class SentimentUtil {
    public static UserSentiment toUserSentiment(SentimentDto dto, User user, int wordsCount) {
        return UserSentiment.builder()
                .score(dto.getScore())
                .date(new Date())
                .userId(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .wordCount(wordsCount)
                .build();
    }

    public static SentimentDictionary toSentimentDictionary(SentimentChooseDto sentimentChooseDto) {
        return SentimentDictionary.builder()
                .sentiment(sentimentChooseDto.getSentiment().getSentimentInt())
                .word(sentimentChooseDto.getWord())
                .wordCount(1)
                .build();
    }

    public static void updateSentimentDictionary(SentimentDictionary sentimentDictionary, SentimentChooseDto sentimentChooseDto) {
        int wordsCount = sentimentDictionary.getWordCount();
        double sentimentDouble = sentimentDictionary.getSentiment();
        double result = ((wordsCount * sentimentDouble) + sentimentChooseDto.getSentiment().getSentimentInt()) / wordsCount + 1;
        sentimentDictionary.setSentiment(result);
        sentimentDictionary.setWordCount(wordsCount + 1);
    }
}
