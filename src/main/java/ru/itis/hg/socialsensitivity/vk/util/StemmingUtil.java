package ru.itis.hg.socialsensitivity.vk.util;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringReader;

public class StemmingUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(StemmingUtil.class);

    public static String stem(String text, String language) {
        StringBuffer result = new StringBuffer();
        if (text != null && text.trim().length() > 0) {
            StringReader tReader = new StringReader(text);
            SnowballAnalyzer analyzer = new SnowballAnalyzer(Version.LUCENE_31, language);
            TokenStream tStream = analyzer.tokenStream("contents", tReader);
            TermAttribute term = tStream.addAttribute(TermAttribute.class);

            try {
                while (tStream.incrementToken()) {
                    result.append(term.term());
                    result.append(" ");
                }
            } catch (IOException ioe) {
                LOGGER.error("Error: " + ioe.getMessage());
            }
        }
        return result.length() == 0 ? text : result.toString().trim();
    }
}
