package ru.itis.hg.socialsensitivity.vk.util;

import ru.itis.hg.socialsensitivity.vk.dto.UserInfoDto;
import ru.itis.hg.socialsensitivity.vk.entity.User;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UserUtil {
    public static User toUser(UserInfoDto userInfoDto) {
        User user = User.builder()
                .firstName(userInfoDto.getFirstName())
                .lastName(userInfoDto.getLastName())
                .build();
        user.setId(userInfoDto.getId());
        return user;
    }

    public static List<User> toUsers(UserInfoDto[] userInfoDtos) {
        return Arrays.stream(userInfoDtos)
                .filter(userInfoDto -> !userInfoDto.getFirstName().equals("DELETED"))
                .map(UserUtil::toUser)
                .collect(Collectors.toList());
    }
}
