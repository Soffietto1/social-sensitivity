package ru.itis.hg.socialsensitivity.vk.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Azat Khayrullin
 */
@Component
public class VkApi {
    private static final String PARAM_SEPARATOR = "&";

    @Value("${vk.api.app_id}")
    private String appId;
    @Value("${vk.api.client.secret}")
    private String appKey;
    @Value("${vk.api.redirect.uri}")
    private String redirectUri;
    @Value("${vk.api.base.url}")
    private String BASE_URL;
    private String userToken;

    public String getFullAuthUri() {
        return "https://oauth.vk.com/authorize?" +
                "client_id=" + appId +
                PARAM_SEPARATOR +
                "display=page" +
                PARAM_SEPARATOR +
                "redirect_uri=" + redirectUri +
                PARAM_SEPARATOR +
                "scope=friends,wall" +
                PARAM_SEPARATOR +
                "response_type=code";
    }

    public String getAccessTokeUri(String code) {
        return "https://oauth.vk.com/access_token?" +
                "client_id=" + appId +
                PARAM_SEPARATOR +
                "client_secret=" + appKey +
                PARAM_SEPARATOR +
                "redirect_uri=" + redirectUri +
                PARAM_SEPARATOR +
                "code=" + code;
    }

    public String getFindUserUri(String ids) {
        return getBaseUrlWithTokenAndVersion("users.get") +
                PARAM_SEPARATOR +
                "user_ids=" + ids;
    }

    public String getFriendsUri(String userid, int count) {
        return getBaseUrlWithTokenAndVersion("friends.get") +
                PARAM_SEPARATOR +
                "user_id=" + userid +
                PARAM_SEPARATOR +
                "order=hints" +
                PARAM_SEPARATOR +
                "count=" + count +
                PARAM_SEPARATOR +
                "offset=0" +
                PARAM_SEPARATOR +
                "fields=domain" +
                PARAM_SEPARATOR +
                "name_case=nom";
    }


    public String getDialogHistoryUrl(Long peerId) {
        return getBaseUrlWithTokenAndVersion("messages.getHistory") +
                PARAM_SEPARATOR +
                "offset=0" +
                PARAM_SEPARATOR +
                "count=200" +
                PARAM_SEPARATOR +
                "extended=0" +
                PARAM_SEPARATOR +
                "peer_id=" + peerId;

    }

    public String getUserPostCommentUri(Long ownerId, Long postId) {
        return getBaseUrlWithTokenAndVersion("wall.getComments") +
                PARAM_SEPARATOR +
                "owner_id=" + ownerId +
                PARAM_SEPARATOR +
                "post_id=" + postId +
                PARAM_SEPARATOR +
                "need_likes=0" +
                PARAM_SEPARATOR +
                "count=100";

    }

    public String getUserPostUri(String ownerId) {
        return getBaseUrlWithTokenAndVersion("wall.get") +
                PARAM_SEPARATOR +
                "owner_id=" + ownerId;
    }

    private String getBaseUrlWithTokenAndVersion(String method) {
        return BASE_URL +
                 method + "?" +
                PARAM_SEPARATOR +
                "access_token=" + userToken +
                PARAM_SEPARATOR +
                "v=5.103";
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }
}
